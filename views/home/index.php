<div>
<?php foreach($this->channels as $channel): ?>
	<?php $channelId = explode('.', $channel['key'])[0]; ?>
	<a href="/epg/home/<?= $channelId ?>" class="panel panel-default col-xs-12 col-sm-6 col-md-4 col-lg-2"><?= $channel['name'] ?></a>
<?php endforeach; ?>
</div>
