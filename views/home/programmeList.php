<?php 
    $begin = new DateTime(date('Y-m-d'));
    $end = new DateTime(date('Y-m-d'));
    $end->add(new DateInterval('P5D'));
    $interval = DateInterval::createFromDateString('1 day');
    $daterange = new DatePeriod($begin, $interval, $end);
?>
<h2><a href="http://port.hu<?= $this->channel['url'] ?>" target="_blank"><?= $this->channel['name'] ?></a> - <?= $this->selectedDate ?></h2>
<ul class="list-inline">
<?php foreach($daterange as $date): ?>
	<?php $channelName = explode('.', $this->channel['key'])[0]; ?>
    <li class="list-group-item"><a href="/epg/home/<?= $channelName.'/'.$date->format('Ymd') ?>"><?= $date->format("Y-m-d") ?></a></li>
<?php endforeach; ?>
</ul>
<ul class="list-group">
<?php if (!empty($this->programme)): ?>
<?php foreach($this->programme as $programme): ?>
	<li class="list-group-item">
		<div><?= date('H:i', strtotime($programme['start_date'])) ?> - <?= date('H:i', strtotime($programme['end_date'])) ?></div>
		<div><a href="http://port.hu<?= $programme['url'] ?>" target="_blank"><?= $programme['title'] ?></a></div>
		<div><?= $programme['desc'] ?></div>
	</li>
<?php endforeach; ?>
<?php else: ?>
	<span>Nincs megjeleníthető műsor a választott napra!</span>
<?php endif; ?>
</ul>
