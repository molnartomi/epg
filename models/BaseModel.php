<?php
namespace Models;

use Components\SQLiteConnection;

abstract class BaseModel
{
    private $pdo;
    
    public function __construct()
    {
        $this->pdo = (new SQLiteConnection())->connect();
    }
    
    public function exec($sql)
    {
        $result = $this->pdo->exec($sql);
        if (!$result){
            error_log(print_r($this->pdo->errorInfo(), true), 3, 'error.log');
        }
    }
    
    public function query($sql)
    {
        $result = null;
        try {
            $result = $this->pdo->query($sql);
        } catch(\PDOException $e) {
            error_log($e->getMessage(), 3, 'error.log');
        }
        return $result;
    }
}
