<?php
namespace Models;

class Channels extends BaseModel
{
    const TABLE_NAME = 'channels';

    public function createTable()
    {
        $sql = "CREATE TABLE ".self::TABLE_NAME." (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            key VARCHAR(50) NOT NULL UNIQUE ON CONFLICT IGNORE,
            name VARCHAR(50) NOT NULL,
            url VARCHAR(127) NOT NULL);";
        $this->exec($sql);
    }
    
    public function insertRecord($values = array())
    {
        $sql="INSERT INTO ".self::TABLE_NAME." (key, name, url)
            VALUES ('".$values['key']."', '".$values['name']."', '".$values['url']."');";
        $this->exec($sql);
    }
    
    public function getAll()
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME.";";
        $query = $this->query($sql);
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getChannel($key)
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME." WHERE key = '".$key."';";
        $query = $this->query($sql);
        $result = $query->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }
}