<?php
namespace Models;

class Programmes extends BaseModel
{
    const TABLE_NAME = 'programmes';
   
    public function createTable()
    {
        $sql = "CREATE TABLE IF NOT EXISTS ".self::TABLE_NAME." (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            channel_id INT(11) NOT NULL,
            start_date DATETIME NOT NULL,
            end_date DATETIME NOT NULL,
            title VARCHAR(127) NOT NULL,
            desc VARCHAR(255) NOT NULL,
            url VARCHAR(127) NOT NULL,
            UNIQUE(channel_id, start_date) ON CONFLICT REPLACE);";
        $this->exec($sql);
    }
    
    public function insertRecord($values = array())
    {
        $sql="INSERT INTO ".self::TABLE_NAME." (channel_id, start_date, end_date, title, desc, url)
            VALUES (".$values['channel_id'].
            ", '".$values['start_date'].
            "', '".$values['end_date'].
            "','".$values['title'].
            "', '".$values['desc'].
            "', '".$values['url']."');";
        $this->exec($sql);
    }
    
    public function getAll()
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME.";";
        $query = $this->query($sql);
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getProgramme($channelId, $date)
    {
        $sql = 'SELECT p.* FROM '.self::TABLE_NAME.' p LEFT JOIN '.
            Channels::TABLE_NAME.' c ON p.channel_id = c.id WHERE c.key="'.$channelId.'.hu.xmltv.czo.hu" AND start_date >= "'
            .$date.' 00:00:00" AND start_date <= "'.$date.' 23:59:59";';
        $query = $this->query($sql);
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }
}