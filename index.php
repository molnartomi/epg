<?php
require 'vendor/autoload.php';
 
use Components\Loader;

$loader = new Loader($_GET);
$controller = $loader->createController();
if ($controller) {
    $controller->executeAction();
}
