<?php
namespace Controllers;

use Models\Channels;
use Models\Programmes;

class HomeController extends BaseController
{
    protected function Index()
    {
        $channel = new Channels();
        $this->returnView('index', [
            'channels' => $channel->getAll(),
        ]);
    }
    
    protected function Dunaworld()
    {
        $this->displayChannelProgrammes();
    }
    
    protected function Rtlklub()
    {
        $this->displayChannelProgrammes();
    }
    
    protected function Dunatv()
    {
        $this->displayChannelProgrammes();
    }
    
    protected function Tv2()
    {
        $this->displayChannelProgrammes();
    }
    
    protected function Viasat3()
    {
        $this->displayChannelProgrammes();
    }
    
    private function displayChannelProgrammes()
    {
        $date = (isset($_GET['id']) && !empty($_GET['id'])) ? date('Y-m-d', strtotime($_GET['id'])) : date('Y-m-d');
        $programme = new Programmes();
        $channel = new Channels();
        $this->returnView('programmeList', [
            'selectedDate' => $date,
            'channel' => $channel->getChannel($this->action.'.hu.xmltv.czo.hu'),
            'programme' => $programme->getProgramme($this->action, $date),
        ]);
    }
}
