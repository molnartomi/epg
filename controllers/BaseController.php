<?php
namespace Controllers;

abstract class BaseController
{
    protected $urlvalues;
    protected $action;
    public $view;

    public function __construct($action, $urlvalues)
    {
        $this->action = $action;
        $this->urlvalues = $urlvalues;
    }

    public function executeAction()
    {
        return $this->{$this->action}();
    }

    protected function returnView($viewName, $params)
    {
        $reflect = new \ReflectionClass($this);
        $folderName = lcfirst(str_replace('Controller', '', $reflect->getShortName()));
        $viewFile = 'views/' . $folderName . '/' . $viewName . '.php';
        if (file_exists($viewFile)){
            $this->addProperty($params);
            $this->view = $this->getViewData($viewFile);
            require('views/template.php');
        }
    }
    
    private function getViewData($viewFile)
    {
        ob_start();
        include $viewFile;
        $template = ob_get_contents();
        ob_end_clean();
        return $template;
    }
    
    private function addProperty($params = array())
    {
        foreach ($params as $proertyKey => $proertyVal) {
            $this->$proertyKey = $proertyVal;
        }
    }
}
