Tv műsor letöltő és listázó - tesztfeladat
==========================================

DIRECTORY STRUCTURE
-------------------

      assets/             css és js file-ok
      components/         segéd file-ok (helpers)
      controllers/        controller osztályok
      db/                 SQLite db file
      models/             model osztályok
      vendor/             composer autoloading fileok
      views/              view fileok
      index.php           entry script
      downloader.php      tv műsor letöltő (console command)

INSTALLATION
------------

      File-ok bemásolása a webroot-ba. Pl.: xampp esetén xampp/htdocs/epg könyvtárba
      Tv műsorok listázása: http://localhost/epg
      Tv műsorok letöltése (alapértelmezetten aktuális nap): downloader.php script futtatása parancssorból vagy http://localhost/epg/downloader.php
      Tv műsor letöltő paraméterezés: TvProgrammeDownloader osztály saveData metódus paraméterül várja a letöltendő nap dátumát YYYY-MM-DD formában
