<?php
namespace Components;

class Loader
{
    private $controller;
    private $action;
    private $urlvalues;

    public function __construct($urlvalues)
    {
        $this->urlvalues = $urlvalues;
        if ($this->urlvalues['controller'] == '') {
            $this->controller = 'home';
        } else {
            $this->controller = $this->urlvalues['controller'];
        }
        if ($this->urlvalues['action'] == '') {
            $this->action = 'index';
        } else {
            $this->action = $this->urlvalues['action'];
        }
    }

    public function createController()
    {
        $className = '\\Controllers\\'.ucfirst($this->controller).'Controller';
        if (class_exists($className)) {
            $parents = class_parents($className);
            if (in_array('Controllers\\BaseController', $parents)) {
                if (method_exists($className, $this->action)) {
                    return new $className($this->action, $this->urlvalues);
                } else {
                    echo ("badUrl: ".$this->controller.'/'.$this->action);
                }
            } else {
                echo ("badUrl: ".$this->controller.'/'.$this->action);
            }
        } else {
            echo ("badUrl: ".$this->controller.'/'.$this->action);
        }
    }
}
