<?php
namespace Components;

use Models\Channels;
use Models\Programmes;

class TvProgrammeDownloader
{
    private $date;
    
    public function saveData($date = null)
    {
        header('Content-type: text/html; charset=UTF-8');
        $this->date = is_null($date) ? date('Y-m-d') : $date;
        $rtlKlub = 'rtlklub.hu.xmltv.czo.hu';
        $tv2 = 'tv2.hu.xmltv.czo.hu';
        $viasat3 = 'viasat3.hu.xmltv.czo.hu';
        $duna = 'dunatv.hu.xmltv.czo.hu';
        $dunaWord = 'dunaworld.hu.xmltv.czo.hu';
        
        $url = 'http://xmltv.czo.hu/get/5/?channels='.$rtlKlub.'+'.$tv2.'+'.$viasat3.'+'.$duna.'+'.$dunaWord;
        $xml = $this->getTvGuide($url);
        if ($xml) {
            $response = simplexml_load_string($xml);
            $this->saveChannels($response->channel);
            $this->saveProgrammes($response->programme);
        }
    }
    
    private function getTvGuide($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            error_log('Curl error: ' . curl_error($ch), 3, 'error.log');
        }
        curl_close($ch);
        return $response;
    }
    
    private function saveChannels($channels)
    {
        $channel = new Channels();
        foreach ($channels as $item) {
            $values = [
                'key' => $item['id'],
                'name' => $item->{'display-name'},
                'url' => $item->url
            ];
            $channel->insertRecord($values);
        }
    }
    
    private function saveProgrammes($programmes)
    {
        $channel = new Channels();
        $programme = new Programmes();
        foreach ($programmes as $item) {
            if (date('Y-m-d', strtotime($item['start'])) == $this->date) {
                $oneChannel = $channel->getChannel($item['channel']);
                $values = [
                    'channel_id' => $oneChannel['id'],
                    'start_date' => date('Y-m-d H:i:s', strtotime($item['start'])),
                    'end_date' => date('Y-m-d H:i:s', strtotime($item['stop'])),
                    'title' => $item->title,
                    'desc' => $item->desc,
                    'url' => $item->url
                ];
                $programme->insertRecord($values);
            }
        }
    }
}

